# Studio Classroom Interview Project (Backend Developer)

空中英語教室 後端工程師面試試題


## 說明

- 本試題共有兩個部分，請依序回答
- 請收到 Email 後 48 小時內作答完畢，並將答案透過 Email 附件夾檔回傳  


### PART(1)
題目1：請參閱php_sql.php範例, 用PHP寫出SQL語法中的 新增/刪除/修改。

題目2：請參閱php_json.php範例使用PHP程式,秀印出JSON格式

題目3：請解釋底下這兩行Cron 指令所代表的意思? 

```
[user]$ crontab -l
0 2 * * * php /var/www/html/mshop/releaseSession.php
```

題目4：請參照php_session-cookie.php，使用PHP程式完成下列要求：
目的是呈現對於session及cookie的瞭解及運用，

    4A. 取得輸入值並列印。
    4B. 將值存入session中，同時也將值存入cookie中，並使cookie值有效3小時。
    4C. 若session值仍存在則列印session值，若session值不存在則由cookie取得其值。




### 備註

- 此專案可以使用任何函式庫或框架來完成。
- 製作過程中，可以任意搜尋相關文件，但請勿與他人討論或分享。